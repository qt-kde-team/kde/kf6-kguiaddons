Source: kf6-kguiaddons
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               cmake (>= 3.16~),
               doxygen,
               extra-cmake-modules (>= 6.11.0~),
               libwayland-dev (>= 1.9~) [linux-any],
               libx11-dev,
               libxcb1-dev,
               libxkbcommon-dev,
               pkgconf,
               plasma-wayland-protocols (>= 1.15.0~) [linux-any],
               qt6-base-dev (>= 6.6.0~),
               qt6-base-private-dev (>= 6.6.0~),
               qt6-declarative-dev,
               qt6-tools-dev (>= 6.5.0~),
               qt6-wayland-dev (>= 6.6.0~) [linux-any],
Standards-Version: 4.7.2
Homepage: https://invent.kde.org/frameworks/kguiaddons
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kf6-kguiaddons
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kf6-kguiaddons.git
Rules-Requires-Root: no

Package: libkf6guiaddons-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Breaks: libkf5guiaddons-bin,
Replaces: libkf5guiaddons-bin,
Description: geographical URI helper
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.
 .
 This package contains kde-geo-uri-handler for opening geographical URIs.

Package: libkf6guiaddons-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: data files for the kguiaddons framework
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.
 .
 This package contains architecture-independent shared data files for the
 kguiaddons framework.

Package: libkf6guiaddons-dev
Section: libdevel
Architecture: any
Depends: libkf6coreaddons-dev (>= 6.7.0~),
         libkf6guiaddons6 (= ${binary:Version}),
         qt6-base-dev (>= 6.6.0+dfsg~),
         qt6-wayland-dev (>= 6.6.0~) [linux-any],
         ${misc:Depends},
Recommends: libkf6guiaddons-doc (= ${source:Version}),
Description: development headers for the kguiaddons framework
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.
 .
 This package contains development files for building software that uses
 libraries from the kguiaddons framework.

Package: libkf6guiaddons-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: documentation files for the kguiaddons framework
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.
 .
 This package contains the qch documentation files for the kguiaddons
 framework.

Package: libkf6guiaddons6
Architecture: any
Multi-Arch: same
Depends: libkf6guiaddons-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: libkf6guiaddons-bin (= ${binary:Version}),
Description: utilities for graphical user interfaces
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.

Package: qml6-module-org-kde-guiaddons
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: QML binding for the kguiaddons framework
 The KDE GUI addons provide utilities for graphical user interfaces in the
 areas of colors, fonts, text, images, keyboard input.
 .
 This package contains the QML bindings for using KDE GUI addons inside QML
 applications.
